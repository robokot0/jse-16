package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.Collections;
import java.util.List;

/**
 * Сервис задач
 */
public class TaskService {
    /**
     * Репозитарий задач
     */
    private final TaskRepostory taskRepostory;

    /**
     * Конструтор
     * @param taskRepostory Репозитарий задач
     */
    public TaskService(final TaskRepostory taskRepostory) {
        this.taskRepostory = taskRepostory;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        return taskRepostory.create(name,userId);
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name, final String description,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание задачи");
        return taskRepostory.create(name, description, userId);
    }

    /**
     * Измениь задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task update(final Long id, final String name, final String description,final Long userId) throws WrongArgumentException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание задачи");
        return taskRepostory.update(id, name, description, userId);
    }

    /**
     * Удалить все задачи
     */
    public void clear() {
        taskRepostory.clear();
    }

    /**
     * Удалить все задачи пользователя
     */
    public void clear(final Long userId ) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        taskRepostory.clear(userId);
    }

    /**
     * Найти по индексу
     * @param index индекс
     * @return задача
     */
    public Task findByIndex(final int index,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException {
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.findByIndex(index);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача по индексу");
        return task;
    }

    /**
     * мНайти по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByIndex(final int index,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.findByIndex(index,userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача пользователя по индексу");
        return task;
    }

    /**
     * Найти по имени
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name,final boolean throwIfNotFound) throws WrongArgumentException, TaskNotFoundException{
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.findByName(name);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача по имени");
        return task;
    }

    /**
     * Найти по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByName(final String name,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.findByName(name);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача пользователя по имени");
        return task;
    }

    /**
     * Найти по идентификатору
     * @param id идентификатор
     * @return задча
     */
    public Task findById(final Long id,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findById(id);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача по ИД");
        return task;
    }

    /**
     * Найти по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задча
     */
    public Task findById(final Long id,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException  {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findById(id,userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача пользователя по ИД");
        return task;
    }

    /**
     * Удалить по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final int index) throws WrongArgumentException, TaskNotFoundException {
        if (index < 0) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeByIndex(index);
        if(task==null) throw new TaskNotFoundException("Не найдена задача по индексу");
        return task;
    }

    /**
     * Удалить по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByIndex(final int index,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index < 0)  throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Task task = taskRepostory.removeByIndex(index,userId);
        if(task==null) throw new TaskNotFoundException("Не найдена задача пользователя по индексу");
        return task;
    }

    /**
     * Удалить по идентификаотру
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id) throws WrongArgumentException, TaskNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeById(id);
        if(task==null) throw new TaskNotFoundException("Не найдена задача по ИД");
        return task;
    }

    /**
     * Удалить по идентификаотру и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeById(final Long id,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.removeById(id,userId);
        if(task==null) throw new TaskNotFoundException("Не найдена задача пользователя по ИД");
        return task;
    }

    /**
     * Удалить по имени
     * @param name имя
     * @return задача
     */
    public Task removeByName(final String name) throws WrongArgumentException, TaskNotFoundException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.removeByName(name);
        if(task==null) throw new TaskNotFoundException("Не найдена задача по имени");
        return task;
    }

    /**
     * Удалить по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByName(final String name,final Long userId) throws WrongArgumentException, TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя задачи");
        final Task task = taskRepostory.removeByName(name,userId);
        if(task==null) throw new TaskNotFoundException("Не найдена задача пользователя по имени");
        return task;
    }

    /**
     * Получить все задачи проекта
     * @param projectId
     * @return список задач
     */
    public List<Task> findAllByProjectId(Long projectId) throws WrongArgumentException {
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId);
    }

    /**
     * Получить все задачи проекта с учетом пользователя
     * @param projectId
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        return taskRepostory.findAllByProjectId(projectId,userId);
    }

    /**
     * Получить все задачи
     * @return список задач
     */
    public List<Task> findAll() {
        return taskRepostory.findAll();
    }

    /**
     * Получить все задачи пользователя
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAll(final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return taskRepostory.findAll(userId);
    }

    /**
     * Получить задачу по проекту и ид
     * @param projectId ид проекта
     * @param id ид
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (projectId == null)  throw new WrongArgumentException("Не задано ИД проекта");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId, id);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача пользователя по ИД в проекте");
        return task;
    }

    /**
     * Получить задачу по проекту ид и пользователю.
     * @param projectId ид проекта
     * @param id ид
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(Long projectId, Long id, final Long userId,final boolean throwIfNotFound) throws WrongArgumentException,TaskNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (projectId == null) throw new WrongArgumentException("Не задано ИД проекта");
        if (id == null) throw new WrongArgumentException("Не задано ИД задачи");
        final Task task = taskRepostory.findByProjectIdAndId(projectId, id, userId);
        if(task==null && throwIfNotFound) throw new TaskNotFoundException("Не найдена задача пользователя по ИД в проекте");
        return task;
    }

}
