package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.entity.Project;

import java.util.List;

/**
 * Сервис проектов
 */
public class ProjectService {
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository;

    /**
     * Конструктор
     * @param projectRepository Репозитарий проектов.
     */
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создать проект
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project create(final String name, final String description,final Long userId) throws WrongArgumentException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание проекта");
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.create(name, description,userId);
    }

    /**
     * Изменить проект
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project update(final Long id, final String name, final String description,final Long userId) throws WrongArgumentException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        if (description == null || description.isEmpty()) throw new WrongArgumentException("Не задано описание проекта");
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.update(id, name, description,userId);
    }

    /**
     * Удалить все проекты
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Удалить все проекты пользователя
     * @param userId ид пользователя
     */
    public void clear(final Long userId) throws WrongArgumentException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        projectRepository.clear(userId);
    }

    /**
     * Найти проект по индексу
     * @param index индекс
     * @return проект
     */
    public Project findByIndex(final int index,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException{
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.findByIndex(index);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("Не найден проект по индексу");
        return project;
    }

    /**
     * Найти проект по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByIndex(final int index,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.findByIndex(index,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("Не найден проект пользователя по индексу");
        return project;
    }
    /**
     * Найти проект по итмени
     * @param name имя
     * @return проект
     */
    public Project findByName(final String name,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.findByName(name);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("Не найден проект по имени");
        return project;
    }

    /**
     * Найти проект по итмени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByName(final String name,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.findByName(name,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("Не найден проект пользователя по имени");
        return project;
    }

    /**
     * Найти проект про идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project findById(final Long id,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException  {
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.findById(id);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("Не найден проект по ИД");
        return project;
    }

    /**
     * Найти проект про идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project findById(final Long id,final Long userId,final boolean throwIfNotFound) throws WrongArgumentException, ProjectNotFoundException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.findById(id,userId);
        if(project==null && throwIfNotFound) throw new ProjectNotFoundException("Не найден проект пользователя по ИД");
        return project;
    }

    /**
     * Удалитьпроект по индексу
     * @param index индекс
     * @return проект
     */
    public Project removeByIndex(final int index) throws WrongArgumentException, ProjectNotFoundException {
        if (index < 0) throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.removeByIndex(index);
        if(project==null) throw new ProjectNotFoundException("Не найден проект по индексу");
        return project;
    }

    /**
     * Удалитьпроект по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByIndex(final int index,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (index < 0)  throw new WrongArgumentException("Индекс должен быть больше нуля");
        final Project project = projectRepository.removeByIndex(index,userId);
        if(project==null) throw new ProjectNotFoundException("Не найден проект пользователя по индексу");
        return project;
    }

    /**
     * Удалить проект по идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project removeById(final Long id) throws WrongArgumentException, ProjectNotFoundException {
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.removeById(id);
        if(project==null) throw new ProjectNotFoundException("Не найден проект по ИД");
        return project;
    }
    /**
     * Удалить проект по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeById(final Long id,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (id == null) throw new WrongArgumentException("Не задано ИД проекта");
        final Project project = projectRepository.removeById(id,userId);
        if(project==null) throw new ProjectNotFoundException("Не найден проект пользователя по ИД");
        return project;
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return проект
     */
    public Project removeByName(final String name) throws WrongArgumentException, ProjectNotFoundException {
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.removeByName(name);
        if(project==null) throw new ProjectNotFoundException("Не найден проект пользователя по ИД");
        return project;
    }

    /**
     * Удалить проект по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByName(final String name,final Long userId) throws WrongArgumentException, ProjectNotFoundException {
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        if (name == null || name.isEmpty()) throw new WrongArgumentException("Не задано имя проекта");
        final Project project = projectRepository.removeByName(name,userId);
        if(project==null) throw new ProjectNotFoundException("Не найден проект пользователя по ИД");
        return project;
    }

    /**
     * Получить все проекты
     * @return список проектов
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Получить все проекты пользователя
     * @param userId ид пользователя
     * @return список проектов
     */
    public List<Project> findAll(final Long userId) throws WrongArgumentException{
        if (userId == null) throw new WrongArgumentException("Не задано ид пользователя");
        return projectRepository.findAll(userId);
    }
}
