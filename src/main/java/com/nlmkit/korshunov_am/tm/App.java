package com.nlmkit.korshunov_am.tm;

import com.nlmkit.korshunov_am.tm.controller.*;
import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.exceptions.MessageException;
import com.nlmkit.korshunov_am.tm.repository.CommandHistoryRepository;
import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.repository.TaskRepostory;
import com.nlmkit.korshunov_am.tm.repository.UserRepository;
import com.nlmkit.korshunov_am.tm.service.*;

import java.util.Scanner;

import static com.nlmkit.korshunov_am.tm.TerminalConst.*;

/**
 *  Тестовое приложение
 */
public class App {
    /**
     * Репозиторий для истории комманд
     */
    private final CommandHistoryRepository сommandHistoryRepository = new CommandHistoryRepository();
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository = new ProjectRepository();
    /**
     * Репозитарий задач.
     */
    private final TaskRepostory taskRepostory = new TaskRepostory();
    /**
     * Репозитарий пользователей.
     */
    private final UserRepository userRepository = new UserRepository();
    /**
     * Сервис истории комманд.
     */
    private final CommandHistoryService commandHistoryService = new CommandHistoryService(сommandHistoryRepository);
    /**
     * Сервис проектов.
     */
    private final ProjectService projectService = new ProjectService(projectRepository);
    /**
     * Сервис задач.
     */
    private final TaskService taskService = new TaskService(taskRepostory);
    /**
     * Сервис задачи в проекте
     */
    private final ProjectTaskService projectTaskService = new ProjectTaskService(projectRepository,taskRepostory);
    /**
     * Сервис пользователй
     */
    private final UserService userService = new UserService(userRepository);
    /**
     * Контролелер системных комманд.
     */
    private final CommandHistoryController commandHistoryController = new CommandHistoryController(commandHistoryService);
    /**
     * Контролелер проектов.
     */
    private final ProjectController projectController = new ProjectController(projectService,commandHistoryService);
    /**
     * Контролер задач.
     */
    private final TaskController taskController = new TaskController(taskService, projectTaskService,commandHistoryService);
    /**
     * Контролер пользователей.
     */
    private final UserController userController = new UserController(userService,projectController,taskController,commandHistoryService);
    /**
     * Контроллер системных комманд.
     */
    private final SystemController systemController = new SystemController(commandHistoryService);

    /**
     * Тестовые данные.
     */
    {
        try {

            final User user1=userRepository.create("USER",Role.valueOf("USER"),"FN","SN","MN",userController.getStringHash("123"));
            final User user2=userRepository.create("USER1",Role.valueOf("USER"),"FN1","SN1","MN1",userController.getStringHash("123"));
            final User admin=userRepository.create("ADMIN",Role.valueOf("ADMIN"),"FN","SN","MN",userController.getStringHash("123"));

            final Project project1 = projectRepository.create("P2","D1",user1.getId());
            final Project project2 = projectRepository.create("P1","D2",user2.getId());

            final Task task1 = taskRepostory.create("T3","D1",user1.getId());
            final Task task2 = taskRepostory.create("T2","D2",user2.getId());
            final Task task3 = taskRepostory.create("T1","D3",user1.getId());

            projectTaskService.addTaskToProject(project1.getId(),task1.getId());
            projectTaskService.addTaskToProject(project2.getId(),task2.getId());

        }
        catch (MessageException e) {
            System.out.println("Ошибка создания тестовых данных " + e.getMessage());
        }


    }

    /**
     * Точка входа
     * @param args Аргументы коммандной строки.
     */
    public static void main(String[] args) {
        final Scanner scanner = new Scanner(System.in);
        final App app = new App();
        app.run(args);
        app.systemController.displayWelcome();
        String command = "";
        while (!(EXIT.equals(command) || SHORT_EXIT.equals(command))) {
            command = scanner.nextLine();
            app.run(command);
        }
    }

    /**
     * Выполнение комманды из аргументов командной строки вызова программы
     * @param args Аргументы коммандной строки.
     */
    public void run(final String[] args) {
        if (args == null) return;
        if (args.length <1) return;
        final String param = args[0];
        final int result = run(param);
        System.exit(result);
    }

    /**
     * Полчить ссылку на сервис проектов
     * @return ссылка на сервис проектов
     */
    public ProjectService getProjectService() {
        return projectService;
    }

    /**
     * Получить ссылку на сервис задач
     * @return ссылка на сервис задач
     */
    public TaskService getTaskService() {
        return taskService;
    }

    /**
     * Получить ссылку на сервис задач и проектов
     * @return ссылка на сервис задач и проектов
     */
    public ProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    /**
     * Выполнение комманды
     * @param param Команда
     * @return -1 ошибка 0 Выполнено
     */
    public int run(final String param) {
        if (param == null || param.isEmpty()) return -1;
        commandHistoryService.AddCommandToHistory(param);
        try {
            switch (param) {
                case SHORT_VERSION:
                case VERSION:return systemController.displayVersion();
                case SHORT_ABOUT:
                case ABOUT:return systemController.displayAbout();
                case SHORT_HELP:
                case HELP:return systemController.displayHelp();
                case SHORT_EXIT:
                case EXIT:return systemController.displayExit();

                case SHORT_PROJECT_CREATE:
                case PROJECT_CREATE:return projectController.createProject();
                case SHORT_PROJECT_CLEAR:
                case PROJECT_CLEAR:return projectController.clearProject();
                case SHORT_PROJECT_LIST:
                case PROJECT_LIST:return projectController.listProject();
                case SHORT_PROJECT_VIEW:
                case PROJECT_VIEW:return projectController.viewProjectByIndex();
                case SHORT_PROJECT_REMOVE_BY_ID:
                case PROJECT_REMOVE_BY_ID:return projectController.removeProjectByID();
                case SHORT_PROJECT_REMOVE_BY_NAME:
                case PROJECT_REMOVE_BY_NAME:return projectController.removeProjectByName();
                case SHORT_PROJECT_REMOVE_BY_INDEX:
                case PROJECT_REMOVE_BY_INDEX:return projectController.removeProjectByIndex();
                case SHORT_PROJECT_UPDATE_BY_INDEX:
                case PROJECT_UPDATE_BY_INDEX:return projectController.updateProjectByIndex();

                case SHORT_TASK_CREATE:
                case TASK_CREATE:return taskController.createTask();
                case SHORT_TASK_CLEAR:
                case TASK_CLEAR:return taskController.clearTask();
                case SHORT_TASK_LIST:
                case TASK_LIST:return taskController.listTask();
                case SHORT_TASK_VIEW:
                case TASK_VIEW:return taskController.viewTaskByIndex();
                case SHORT_TASK_REMOVE_BY_ID:
                case TASK_REMOVE_BY_ID:return taskController.removeTaskByID();
                case SHORT_TASK_REMOVE_BY_NAME:
                case TASK_REMOVE_BY_NAME:return taskController.removeTaskByName();
                case SHORT_TASK_REMOVE_BY_INDEX:
                case TASK_REMOVE_BY_INDEX:return taskController.removeTaskByIndex();
                case SHORT_TASK_UPDATE_BY_INDEX:
                case TASK_UPDATE_BY_INDEX:return taskController.updateTaskByIndex();
                case SHORT_TASK_ADD_TO_PROJECT_BY_IDS:
                case TASK_ADD_TO_PROJECT_BY_IDS:return taskController.addTaskToProjectByIds();
                case SHORT_TASK_REMOVE_FROM_PROJECT_BY_IDS:
                case TASK_REMOVE_FROM_PROJECT_BY_IDS:return taskController.removeTaskFromProjectByIds();
                case SHORT_TASK_LISTS_BY_PROJECT_ID:
                case TASK_LISTS_BY_PROJECT_ID:return taskController.listTaskByProjectId();

                case SHORT_USER_CREATE:
                case USER_CREATE:return userController.createUser();
                case SHORT_USER_LIST:
                case USER_LIST:return userController.listUser();

                case SHORT_USER_VIEW_BY_ID:
                case USER_VIEW_BY_ID:return userController.viewUserById();
                case SHORT_USER_VIEW_BY_INDEX:
                case USER_VIEW_BY_INDEX:return userController.viewUserByIndex();
                case SHORT_USER_VIEW_BY_LOGIN:
                case USER_VIEW_BY_LOGIN:return userController.viewUserByLogin();
                case SHORT_USER_REMOVE_BY_ID:
                case USER_REMOVE_BY_ID:return userController.removeUserById();
                case SHORT_USER_REMOVE_BY_INDEX:
                case USER_REMOVE_BY_INDEX:return userController.removeUserByIndex();
                case SHORT_USER_REMOVE_BY_LOGIN:
                case USER_REMOVE_BY_LOGIN:return userController.removeUserByLogin();
                case SHORT_USER_UPDATE_BY_ID:
                case USER_UPDATE_BY_ID:return userController.updateUserDataById();
                case SHORT_USER_UPDATE_BY_INDEX:
                case USER_UPDATE_BY_INDEX:return userController.updateUserDataByIndex();
                case SHORT_USER_UPDATE_BY_LOGIN:
                case USER_UPDATE_BY_LOGIN:return userController.updateUserDataByLogin();
                case SHORT_USER_UPDATE_PASSWORD_BY_ID:
                case USER_UPDATE_PASSWORD_BY_ID:return userController.updateUserPasswordById();
                case SHORT_USER_UPDATE_PASSWORD_BY_INDEX:
                case USER_UPDATE_PASSWORD_BY_INDEX:return userController.updateUserPasswordByIndex();
                case SHORT_USER_UPDATE_PASSWORD_BY_LOGIN:
                case USER_UPDATE_PASSWORD_BY_LOGIN:return userController.updateUserPasswordByLogin();
                case SHORT_USER_AUTH:
                case USER_AUTH:return userController.authUser();
                case SHORT_USER_UPDATE_PASSWORD:
                case USER_UPDATE_PASSWORD:return userController.updateAuthUserPassword();
                case SHORT_USER_VIEW:
                case USER_VIEW:return userController.viewAuthUser();
                case SHORT_USER_UPDATE:
                case USER_UPDATE:return userController.updateAuthUser();
                case SHORT_USER_END:
                case USER_END:return userController.endAuthUserSession();
                case SHORT_USER_OF_TASK_SET_BY_INDEX:
                case USER_OF_TASK_SET_BY_INDEX:return userController.setTaskUserByIndex();
                case SHORT_USER_OF_PROJECT_SET_BY_INDEX:
                case USER_OF_PROJECT_SET_BY_INDEX:return userController.setProjectUserByIndex();
                case SHORT_COMMAND_HISTORY_VIEW:
                case COMMAND_HISTORY_VIEW:return commandHistoryController.listCommandHistory();
                default:
                    commandHistoryService.AddCommandResultToLastCommand("Unknown command");
                    return systemController.displayError();
            }
        }
        catch (MessageException e) {
                commandHistoryController.ShowResult("[FAIL]");
                commandHistoryController.ShowResult(e.getMessage());
                return 0;
            }

    }


}
