package com.nlmkit.korshunov_am.tm.entity;

/**
 * Задача
 */
public class Task {
    /**
     * Идентификатор
     */
    private Long id = System.nanoTime();
    /**
     * Имя
     */
    private String name = "";
    /**
     * Описание
     */
    private String description = "";
    /**
     * Идентификатор проекта
     */
    private Long projectId = null;
    /**
     * Идентификатор польователя владельца задачи
     */
    private Long userId = null;
    /**
     * Констурктор по умолчанию
     */
    public Task() {
    }

    /**
     * Конструктор
     * @param name имя
     */
    public Task(final String name,final Long userId) {
        this.name = name;
        this.userId=userId;
    }

    /**
     * Получить идентификатор
     * @return идентификатор
     */
    public Long getId() {
        return id;
    }

    /**
     * Установить идентификатор
     * @param id идентификатор
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Получить име
     * @return имя
     */
    public String getName() {
        return name;
    }

    /**
     * Установить имя
     * @param name имя
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Получить описание
     * @return описание
     */
    public String getDescription() {
        return description;
    }

    /**
     * Указать описание
     * @param description описание
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Получить идентификатор проекта
     * @return идентификатор проекта
     */
    public Long getProjectId() {
        return projectId;
    }

    /**
     * Установить идентификатор проекта
     * @param projectId идентификатор проекта
     */
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    /**
     * Получить ид пользователя владельца проекта
     * @return ид пользователя владельца проекта
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Установить ид пользователя владельца проекта
     * @param userId ид пользователя владельца проекта
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Получить в виде строки для отображения пользователю
     * @return строка для отображения
     */
    @Override
    public String toString() {
        return id + ": " + name;
    }
}
