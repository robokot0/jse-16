package com.nlmkit.korshunov_am.tm;

import static org.junit.Assert.assertTrue;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;
import com.nlmkit.korshunov_am.tm.exceptions.ProjectNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.TaskNotFoundException;
import com.nlmkit.korshunov_am.tm.exceptions.WrongArgumentException;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue() throws ProjectNotFoundException, TaskNotFoundException, WrongArgumentException {
        final App app = new App();
        final Task task = app.getTaskService().findByIndex(0,true);
        System.out.println(task);
        final Project project =
                app.getProjectService().findByIndex(0,true);
        System.out.println(project);
        app.getProjectTaskService().addTaskToProject(project.getId(),task.getId());
        System.out.println(app.getProjectTaskService().findAllByProjectId(project.getId()));
        app.getProjectTaskService().removeTaskFromProject(project.getId(),task.getId());
        System.out.println(app.getProjectTaskService().findAllByProjectId(project.getId()));
        assertTrue( true );
    }
}
